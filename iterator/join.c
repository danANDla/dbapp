//
// Created by danandla on 3/5/23.
//

#include <string.h>
#include "join.h"
#include "../tableLevel/table_util.h"

bool fieldCmp(uint16_t leftColumn, void *leftField, struct _tbSchema *leftSchema,
              uint16_t rightColumn, void *rightField, struct _tbSchema *rightSchema,
                      enum data_type dataType, FILE* fd) {
    bool res = false;
    switch (dataType) {
        case FIELD_TYPE_STRING: {
            char *left = getStrField(leftColumn, leftSchema, leftField, fd);
            char *right = getStrField(rightColumn, rightSchema, rightField, fd);
            res = strEqualTest(left, right);
            break;
        }
        case FIELD_TYPE_FLOAT32: {
            float left = getFloatField(leftColumn, leftSchema, leftField);
            float right = getFloatField(rightColumn, rightSchema, rightField);
            res = floatEqualTest(&left, &right);
            break;
        }
        case FIELD_TYPE_INT32: {
            int left = getIntField(leftColumn, leftSchema, leftField);
            int right = getIntField(rightColumn, rightSchema, rightField);
            res = intEqualTest(&left, &right);
            break;
        }
        case FIELD_TYPE_BOOL: {
            uint8_t left = getBoolField(leftColumn, leftSchema, leftField);
            uint8_t right = getBoolField(rightColumn, rightSchema, rightField);
            res = boolEqualTest(&left, &right);
            break;
        }
    }
    return res;
}

bool joinCmp(joinOperator *join) {
    return fieldCmp(join->leftColumn, join->leftSrc->base.current, join->leftSrc->schema,
                    join->rightColumn, join->rightSrc->base.current, join->rightSrc->schema,
                    join->column_type, join->leftSrc->fd);
}

bool joinMoveNext(joinOperator *join) {
    while (true) {
        if (join->leftRecord == NULL) {
            if (join->leftSrc->base.moveNext((iterator *) join->leftSrc)) {
                join->leftRecord = join->leftSrc->base.current;
                if (join->rightSrc == NULL)
                    join->rightSrc = tupleIt(join->rightSrcPos, join->leftSrc->fd);
            } else return false;
        }
        while (join->rightSrc->base.moveNext((iterator *) join->rightSrc)) {
            join->rightRecord = join->rightSrc->base.current;
            if (joinCmp(join)) {
                return true;
            }
        }
        join->rightSrc = NULL;
        join->leftRecord = NULL;
    }
}

void joinInit(joinOperator *join, tupleIterator *leftIter, tupleIterator *rightIter, uint16_t leftColumnId,
              uint16_t rightColumnId) {
    FILE *fd = leftIter->fd;
    join->moveNext = joinMoveNext;
    join->leftSrc = leftIter;
    join->rightSrc = rightIter;
    join->leftColumn = leftColumnId;
    join->rightColumn = rightColumnId;
    join->leftSrcPos = leftIter->tablePos;
    join->rightSrcPos = rightIter->tablePos;
    join->leftRecord = NULL;

    enum data_type dataTypeL = leftIter->schema->fields[leftColumnId];
    enum data_type dataTypeR = rightIter->schema->fields[rightColumnId];

    if (dataTypeL != dataTypeR) {
        printf("incomparable types of table's columns");
        join->column_type = dataTypeL;
    } else {
        join->column_type = dataTypeL;
    }

    printf("JOIN on tables:\n");
    struct tableHeader table;
    readTableHeader(fd, leftIter->tablePos, &table);
    int32_t leftId = table.table_id;
    readTableHeader(fd, rightIter->tablePos, &table);
    int32_t rightId = table.table_id;
    printf("%d %16d\n", leftId, rightId);
    printf("%"PRIu16" %16"PRIu16"\n", leftColumnId, rightColumnId);
    printf("%s %8s\n", leftIter->schema->field_labels[leftColumnId], rightIter->schema->field_labels[rightColumnId]);
    char fieldType[10];
    switch (join->column_type) {
        case FIELD_TYPE_STRING:
            strcpy(fieldType, "string");
            break;
        case FIELD_TYPE_FLOAT32:
            strcpy(fieldType, "float");
            break;
        case FIELD_TYPE_INT32:
            strcpy(fieldType, "int");
            break;
        case FIELD_TYPE_BOOL:
            strcpy(fieldType, "bool");
            break;
    }
    printf("%s\n", fieldType);
}