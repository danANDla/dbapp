//
// Created by danandla on 11/15/22.
//

#ifndef DBAPP_TABLE_UTIL_H
#define DBAPP_TABLE_UTIL_H

#include "table_internals.h"
#include "../pageLevel/page_writer.h"
#include "../pageLevel/page_internals.h"
#include "../schema/schema_util.h"

void printTableHeader(const struct tableHeader *header);
void printAllPagesAddresses(FILE* in, const int64_t tablePagePos);
enum readStatus readTableHeader(FILE *in, const int64_t pagePos, struct tableHeader *header);
enum writeStatus updateTableHeader(FILE *out, int64_t tablePos, const struct tableHeader *const table);
enum readStatus getTableById(FILE * const in, struct tableHeader * table, uint64_t id, int64_t* tablePos);
enum writeStatus createTable(FILE* out, struct _tbSchema* schema);
enum writeStatus pushPageToTable(FILE *out, const int64_t tablePagePos);
enum writeStatus deletePageFromTable(FILE *out, const int64_t tablePagePos, uint32_t pageNumber);
enum writeStatus addDataToTable(FILE* out, const uint64_t tableId, void** data);

#endif //DBAPP_TABLE_UTIL_H
