import pandas as pd
import array
import struct
import numpy as np


def fillCocktailBuffer():
    df = pd.read_csv('cocktails', delimiter=',', encoding='UTF-8')

    f = open('bufferCocktails', mode='wb')

    b = bytearray()
    n = len(df.index)
    n = n.to_bytes(4, byteorder='little', signed=False)
    b.extend(n)
    f.write(b)
    for index, row in df.iterrows():
        b = bytearray()

        s = row['cocktailName']
        b.extend(map(ord, s))
        b.extend(b"\x00")

        p = float(row['price'])
        b.extend(struct.pack("f", p))

        s = row['cocktailType']
        b.extend(map(ord, s))
        b.extend(b"\x00")

        f.write(b)


def fillIngredients():
    df = pd.read_csv('ingredients', delimiter=',', encoding='UTF-8')

    f = open('bufferIngredients', mode='wb')

    b = bytearray()
    n = len(df.index)
    n = n.to_bytes(4, byteorder='little', signed=False)
    b.extend(n)
    f.write(b)
    for index, row in df.iterrows():
        b = bytearray()

        s = row['ingredientName']
        b.extend(map(ord, s))
        b.extend(b"\x00")

        # p = int(row['cocktailId'])
        # b.extend(struct.pack("i", p))
        # print(b)

        f.write(b)


def fillRecipes():
    df = pd.read_csv('recipes', delimiter=',', encoding='UTF-8')
    f = open('bufferRecipes', mode='wb')
    b = bytearray()
    n = len(df.index)
    n = n.to_bytes(4, byteorder='little', signed=False)
    b.extend(n)
    f.write(b)
    for index, row in df.iterrows():
        b = bytearray()

        p = int(row['cocktailId'])
        b.extend(struct.pack("i", p))

        p = int(row['ingredientId'])
        b.extend(struct.pack("i", p))

        p = float(row['amount'])
        b.extend(struct.pack("f", p))

        f.write(b)


if __name__ == "__main__":
    fillIngredients()
    fillRecipes()
