//
// Created by danandla on 2/11/23.
//

#ifndef DBAPP_STRING_UTIL_H
#define DBAPP_STRING_UTIL_H

#include "../tableLevel/table_internals.h"
#include "../pageLevel/page_writer.h"

enum readStatus readString(FILE *in, struct tid id, char** str);
enum writeStatus writeString(FILE *out, char *str, struct tid *id);

#endif //DBAPP_STRING_UTIL_H
